from rest_framework import permissions
from django.contrib.auth import get_user_model
from provider.models import Provider
from customer.models import Customer
User = get_user_model()


class ProviderPermission(permissions.BasePermission):
    def has_permission(self, request, view):

        provider_id = view.kwargs['provider_id']
        provider_user_id = Provider.objects.get(id=provider_id).user.id
        user_id = request.user.id
        return bool(user_id == provider_user_id)


class CustomerPermission(permissions.BasePermission):
    def has_permission(self, request, view):

        customer_id = view.kwargs['customer_id']
        customer_user_id = Customer.objects.get(id=customer_id).user.id
        user_id = request.user.id
        return bool(user_id == customer_user_id)
