from datetime import timedelta
import datetime

# appointments = [(datetime(2012, 5, 22, 10), datetime(2012, 5, 22, 10, 30)),
#                 (datetime(2012, 5, 22, 12), datetime(2012, 5, 22, 13)),
#                 (datetime(2012, 5, 22, 15, 30), datetime(2012, 5, 22, 17, 10))]

# hours = (datetime(2012, 5, 22, 9), datetime(2012, 5, 22, 12), datetime(2012, 5, 22, 13), datetime(2012, 5, 22, 19))


appointments = [(datetime.datetime(2020, 10, 11, 10, 0), datetime.datetime(2020, 10, 11, 10, 30)), (datetime.datetime(2020, 10, 11, 13, 0), datetime.datetime(2020, 10, 11, 14, 30))]
hours = ((datetime.datetime(2020, 10, 11, 9, 0), datetime.datetime(2020, 10, 11, 12, 0)),(datetime.datetime(2020, 10, 11, 13, 0), datetime.datetime(2020, 10, 11, 16, 0)) )
def get_slots(hours, appointments, duration=timedelta(hours=0.5)):
    my_list = []
    # x = len(hours)
    # for i in range(0, x):
    #     my_list.append((hours[i], hours[i]))
    # slots = sorted(my_list + appointments)
    # print(slots)
    p = []
    for k in range(0, len(hours)):
        m = hours[k]
        print(m)
        my_list.append((m[0], m[0]))
        my_list.append((m[1], m[1]))
        for u in my_list:
            k = []
            k.append(u)
            slots = sorted(k + appointments)
            # print(slots)

            for start, end in ((slots[i][1], slots[i+1][0]) for i in range(len(slots)-1)):
                assert start <= end, "Cannot attend all appointments"
                while start + duration <= end:
                    print("{:%H:%M} - {:%H:%M}".format(start, start + duration))
                    start=start + duration

get_slots(hours, appointments)

# print(appointments[0])
# print(type(datetime(2012, 5, 22, 10)))


