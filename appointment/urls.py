from django.urls import path, include
from .views import *
from rest_framework.routers import SimpleRouter

appointment = SimpleRouter()
appointment.register('', AppointmentModelViewSet)


urlpatterns = [
    path('<int:customer_id>/', include(appointment.urls)),
    path('', AvailabilityTimeSlot.as_view(), name='empty time slots')


]
