from django.db import models
from customer.models import Customer
from provider.models import  Provider
from availability.models import Availability
from availability.serializers import AvailabilitySerializer

class Appointment(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    appointment_date = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)





    # def provider_availability_slots(self):
    #     provider_id = self.provider_id
    #     provider_time_slots = Availability.objects.filter(provider__id__contains=provider_id)
    #     serializer = AvailabilitySerializer(provider_time_slots)
    #     data = serializer.data
    #     return data

    def __str__(self):
        return str(self.pk)


